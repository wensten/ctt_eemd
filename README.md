# ctt_eemd

#### 介绍
本项目工程是重庆师范大学地理与旅游学院 陈田田 副教授团队研发，主要用于长时间序列的NDVI变化分析，完成了多年数据的噪声、季节性变化和年际变化信号分解工作。eemd算法代码主要参考网络代码，自己将eemd扩展到平面栅格图像中，实现了在matlab平台下面向二维平面空间的EEMD模型运算工作。作者：王强，email:cqwensten@163.com 

#### 软件架构
采用matlab开发，用于长时间序列的数据变量分级（经验模式分解），以提取处噪声信号、季节性变化和年际变化规律。目前已在全国1982-2020年，逐月的ndvi数据分析中进行应用，目前代码在matlab2013a_64bit版本下运行测试。研发完成时间为2022年1月于重庆。


#### 安装教程

1.  安装matlab2013a_64bit软件，建议电脑配置内存在64G以上（并行计算需要）；
2.  切换工作空间到当前工程文件夹；
3.  修改doeemd.m中的输入数据路径和输出结果路径。

#### 使用说明

1.  本工程的程序入口代码为doeemd.m文件。设置模型需要的ndvi文件路径（input_ndvi.tif）以及输出的结果即可。输出结果包括noise.tif（噪音）、annual.tif（季节性）、innerAnnual.tif（年际变化）等。
2.  输入的input_ndvi.tif需要采用envi进行波段layerstatck，形成468多个波段。
3.  本工程采用了matlab并行计算机制，本文的作者运行环境为96G内存，48个线程cpu的服务器，开启了5个并行任务。全中国运行时间在7天左右。通过测试,建议内存32G服务器设置并行任务数为3.
4.  输出结果文件需要手动在envi中设置头文件（hdr），本文输出数据为double。即输出结果中：
samples = 1372；
lines   = 1053；
bands   = 468；
data type = 5；
5. 参考wen文章，分为半年、2年为分界点进行分析。其中小于6为半年，小于24为2年。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
