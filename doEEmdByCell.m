function [pix_imfs, res, noise, annual, innerAnnual] = doEEmdByCell(Y)
ysize = length(Y);

%% 核心代码，开始eemd计算 
Nstd = 0.2 *std(Y);
NE = 50;
pix_imfs = eemd( Y, Nstd, NE);

%% imf分量形状            
imfSZ = size(pix_imfs);
% imf分量个数
imf_No = size(pix_imfs,2);
%fprintf('--> imf_No: %d \n',imf_No-2);

noise = zeros(ysize,1);        %噪音
annual = zeros(ysize,1);       %季节性
innerAnnual = zeros(ysize,1);  %年际变化

imf1 = pix_imfs( :, 1); % 原始输入信号y

for k = 2: imf_No-1
%for k = 1: imf_No
    imf = pix_imfs(:, k);
    %% 计算imf分量的固定周期
    period_i = getImfPeriodTByWen(imf);
    % period_str = num2str(period_i);
    % fprintf('--> period_i: %.4f \n', period_i);
    %% 参考wen文章，分为半年、2年为分界点进行分析
    if period_i < 6
        noise = noise + imf;
    elseif period_i>=6 && period_i<=24
        annual= annual + imf;
    else
        innerAnnual= innerAnnual +imf;
    end 
end

%残余分量
res = pix_imfs( : ,imf_No);

innerAnnual = innerAnnual+ res;

