clc;
clear;
close all;
tic;
%% 读取ENVI TIF图层
imgfilename='E:\temp\ctt\ndvi19822018_monschina.tif';
[datas, samples,lines,bands] = read_ENVIimagefile(imgfilename);
fprintf('----> open envi images');
fprintf('----> the images samples: %d，lines: %d, bands: %d ---- \n',samples,lines,bands);

noise_file='E:\temp\ctt\out\china_noise.tif';
annual_file='E:\temp\ctt\out\china_annual.tif';
innerAnnual_file='E:\temp\ctt\out\china_innerAnnual.tif';

noise_datas = zeros(lines,samples,bands);
annual_datas = zeros(lines,samples,bands);
innerAnnual_datas = zeros(lines,samples,bands);

% 无数据区
nodatas = zeros(bands,1);
for m=1: bands
    nodatas(m) = -1.0;
end

matlabpool local 5
parfor i=1: samples
%for i=1: samples
    for j=1: lines
        fprintf('\n');
        fprintf('--> samples: %d，lines: %d, cellVal: %d ---- \n',i,j,datas(j,i,1));
        cell_val = datas(j, i, :);
        Y = reshape(cell_val, bands, 1);
        
        % 无数据区个数
        len_nodatas = length(find(Y == -1));
        if len_nodatas == 0 && std(Y) ~= 0
          %% 执行该象元的EEMD模型
           [pix_imfs, res, noise, annual, innerAnnual] = doEEmdByCell( Y );
           
           noise_datas(j, i, :) = noise;
           annual_datas(j, i, :) = annual;
           innerAnnual_datas(j, i, :) = innerAnnual ;
        else
           noise_datas(j, i, :) = nodatas;
           annual_datas(j, i, :) = nodatas;
           innerAnnual_datas(j, i, :) = nodatas ;
        end
    end
end

matlabpool close

%% 保存到TIF
multibandwrite(noise_datas, noise_file, 'bsq', 'machfmt', 'ieee-le', 'precision', 'double');
multibandwrite(annual_datas, annual_file, 'bsq', 'machfmt', 'ieee-le', 'precision','double');
multibandwrite(innerAnnual_datas, innerAnnual_file, 'bsq', 'machfmt' ,'ieee-le' , 'precision', 'double');





