function period_i = getImfPeriodTByWen(imf)
period_i = -1;
% 总个数
total_period_size = length(imf);
%x = 1:1:total_period_size;

%% 第一种算法
%极大值
[pk_max,idx_pkmax] = findpeaks( imf, 'minpeakdistance', 3); % pk对应峰值，idx对应峰值位数
max_peaks = length(pk_max);
%极小值
[pk_min,idx_pkmin] = findpeaks( 2-imf , 'minpeakdistance', 3);
min_peaks = length(pk_min);

peak_size = max_peaks + min_peaks;

%% 第二种算法
%idx_min = find(diff(sign(diff(imf)))>0) +1;
idx_max = find(diff(sign(diff(imf)))<0) +1;

%peak_size= length(idx_min)+length(idx_max);
%peak_size = length(idx_max);

period_i = (total_period_size/peak_size)*2;

