clc;
clear;
close all;
tic;
%% 产生仿真信号
fs = 100;   %数据采样率Hz
t = 1: 1/fs : 4096*1/fs; %对数据进行采样
n = length(t);  %数据的采样数目
f1 = 0.25; %信号的频率
f2 = 0.005;
x = 2*sin(2*pi*f1*t + cos(2*pi*f2*t)); %产生原始信号，
x = x';    %将其转置为列向量
nt = 0.2 * randn(n,1);  %高斯白噪声生成
y = x + nt; %含噪信号

figure(1);
subplot(2,1,1);plot(x);title('原始信号');xlabel('采样点数');ylabel('幅值');
subplot(2,1,2);plot(y);title('含噪信号');xlabel('采样点数');ylabel('幅值');
%% EEMD分解

Nstd = 0.2 * std(y);
NE = 30;
%执行eemd
imfs = eemd(y, Nstd, NE);

imf_No = size(imfs, 2);
fprintf('--> imf_No: %d \n', imf_No);
plot_Width = imf_No/2;
figure(2);
% IMF1是原始输入信号y，IMF2-IMF7是从高频到低频的IMF分量，IMF8是残余分量
subplot(4,2,1); plot(imfs(:,1));title('IMF1');xlabel('采样点数');ylabel('幅值');
subplot(4,2,2); plot(imfs(:,2));title('IMF2');xlabel('采样点数');ylabel('幅值');
subplot(4,2,3); plot(imfs(:,3));title('IMF3');xlabel('采样点数');ylabel('幅值');
subplot(4,2,4); plot(imfs(:,4));title('IMF4');xlabel('采样点数');ylabel('幅值');
subplot(4,2,5); plot(imfs(:,5));title('IMF5');xlabel('采样点数');ylabel('幅值');
subplot(4,2,6); plot(imfs(:,6));title('IMF6');xlabel('采样点数');ylabel('幅值');
subplot(4,2,7); plot(imfs(:,7));title('IMF7');xlabel('采样点数');ylabel('幅值');
subplot(4,2,8); plot(imfs(:,8));title('IMF8');xlabel('采样点数');ylabel('幅值');
%for i=1:imf_No
%    subplot(plot_Width, 2, i); plot(imfs(:,i));title('IMF'+num2str(i));xlabel('采样点数');ylabel('幅值');
%end

imf = imfs(:,2:7);
n=6;
r=zeros(1,n);
for i=1:n
    % 求相关系数
    r(1,i) = sum((y-mean(y)).*(imf(:,i)-mean(imf(:,i))),1)./sqrt(sum(((y-mean(y)).^2),1).*sum(((imf(:,i)-mean(imf(:,i))).^2),1));
end
% 求标准差
d=std(r);


% 信号的重构
yt = imf(:,6);
figure(3);
plot(yt);
hold on;
plot(x);
title('去噪信号与原始信号');xlabel('采样点数');ylabel('幅值');
toc;



