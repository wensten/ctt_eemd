function write_envi(input_data,filename,geo)
%保存ENVI文件(.img和.hdr)到内存
%输入
%input_data,图像立方体
%filename，路径加文件名，无扩展名
%geo,地理信息
%

file_img=[filename,'.img'];
file_hdr=[filename,'.hdr'];
%分析文件的行、列、层 及 数据格式
[samples,lines,bands] = size(input_data);
input_data_class = class(input_data);
%写出数据文件
file_img_id = fopen(file_img,'wb');
%output_data = permute(input_data,[2,1,3]);
fwrite(file_img_id, input_data, input_data_class);
fclose(file_img_id);

%写出头文件
switch input_data_class %数据格式
    case 'int16'
    input_data_class_num = 2;
    case 'int32'
    input_data_class_num = 3;
    case 'single'
    input_data_class_num = 4;
    case 'double'
    input_data_class_num = 5;
    case 'Uint16'
    input_data_class_num = 12;
    case 'Uint32'
    input_data_class_num = 13;
    case 'int64'
    input_data_class_num = 14;
    case 'Uint64'
    input_data_class_num = 15;
    otherwise
    errordlg('数据类型错误！','错误');
    return
end
%output_File_hdr = output_File;
%output_File_hdr(end-2:end) = 'hdr';
file_hdr_id = fopen(file_hdr,'wt');
fprintf(file_hdr_id,'%s\n','ENVI');
fprintf(file_hdr_id,'%s\n','description = {');
fprintf(file_hdr_id,'%s\n',' File Imported into ENVI.}');
fprintf(file_hdr_id,'%s\n',['samples = ' num2str(samples)]);
fprintf(file_hdr_id,'%s\n',['lines = ' num2str(lines)]);
fprintf(file_hdr_id,'%s\n',['bands = ' num2str(bands)]);
fprintf(file_hdr_id,'%s\n','header offset = 0');
fprintf(file_hdr_id,'%s\n','file type = ENVI Standard');
fprintf(file_hdr_id,'%s\n',['data type = ' num2str(input_data_class_num)]);
fprintf(file_hdr_id,'%s\n','interleave = bsq');
fprintf(file_hdr_id,'%s\n','sensor type = Unknown');
fprintf(file_hdr_id,'%s\n','byte order = 0');
fprintf(file_hdr_id,'%s\n','sensor type = Unknown');
fprintf(file_hdr_id,'%s\n','wavelength units = Unknown');
if nargin==3
    if isfield(geo,'mapinfo')
        fprintf(file_hdr_id,'%s\n',geo.mapinfo);
    else
        fprintf(file_hdr_id,'%s\n',['map info = {Geographic Lat/Lon,' num2str(geo.image_X) ', ' num2str(geo.image_Y) ', ' num2str(geo.long) ', ' num2str(geo.lat) ', '...
        num2str(geo.long_scale) ', ' num2str(geo.lat_scale) ', WGS-84, units=Degrees}']);
    end
    fprintf(file_hdr_id,'%s\n',geo.coordinate);
end
fclose(file_hdr_id);